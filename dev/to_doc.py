from tomarkdown import py_source_to_token, token_line_to_yaml, yaml_to_md

import pathlib


def main():
    path_in = pathlib.Path('../src_py/polynomial/core.py')
    path_out = pathlib.Path('../docs/pages/')
    path_out.mkdir(parents = True, exist_ok = True)

    # with open(path_in, mode = 'r') as in_stream:
    #     source = in_stream.readlines()

    mds = yaml_to_md('core', token_line_to_yaml(py_source_to_token(path_in)))

    for name, content in mds.items():
        with open(path_out/f'{name}.md', mode = 'w') as out_stream:
            out_stream.write(content)


if __name__ == '__main__': main()
