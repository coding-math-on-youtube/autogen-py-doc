from __future__ import annotations

from typing import Optional


'''
*author*: coadami

**Imports**

    - from typing import Optional
    

**Description**

simplistic library for polynomials in monomial base representation.
'''


class Polynomial(object):
    '''
    the sole class of this package which embodies polynomials in monomial base representation, i.e.

    $$
    p(t) \equiv \sum_{i = 0}^d c_i\cdot (t - t_{\mathrm{ref}})^i
    $$
    '''
    coeff: list[float]
    t_ref: float

    @classmethod
    def const(cls, val: float, degree: int, t_ref: Optional[float] = 0.0) -> Self:
        '''
        pseudo-constructor (or factory method) to construct the constant polynom, where $c_i = 0$, for $i > 0$, s.t.

        $$
        p(t) \equiv \sum_{i = 0}^d c_i\cdot (t - t_{\mathrm{ref}})^i = c_0 = \mathrm{val}
        $$

        **args**

            - val: first or constant coefficient, i.e. $c_0 = \mathrm{val}$
            - degree: the polynomial degree, s.t. $\mathrm{coeff} = (c_0, \dots, c_i, \dots)$ gets created of correct
                length (which is degree + 1)
            - t_ref: the reference point or domain-offset

        **returns**

            - the constant polynomial
        '''
        coeff = [0.0 for _ in range(degree + 1)]
        coeff[0] = val
        return cls(coeff = coeff, t_ref = t_ref)

    def __init__(self, coeff: list[float], t_ref: Optional[float] = 0.0):
        '''
        initializer of the Polynomial-class representing, storing and operating with polynomials in monomial base, i.e.

        $$
        p(t) \equiv \sum_{i = 0}^d c_i\cdot (t - t_{\mathrm{ref}})^i
        $$

        **args**

            - coeff: coefficients, s.t. $\mathrm{coeff} = (c_0, \dots, c_i, \dots)$
            - t_ref: the reference point or domain-offset
        '''
        if len(coeff) == 0: raise AssertionError('len(coeff) > 1!')
        self.coeff = coeff
        self.t_ref = t_ref

    @property
    def degree(self) -> int:
        '''
        **returns**

            - the (max. possible) degree of the stored polynomial

        for the actual degree: subtract number of leading zeros
        '''
        return len(self.coeff) - 1

    def ensure(self, other: Self | float) -> Self:
        '''
        checks whether self and other (provided other is of type "Polynomial") do fit for arithmetical operations.
        Otherwise, if other is of type float then other gets converted into a fitting constant polynomial

        **args**

            - other: other instance of Polynomial or float

        **returns**

            - Polynomial instance
        '''
        if isinstance(other, Polynomial):
            if self.degree != other.degree: raise AssertionError('degree missmatch!')
            if self.t_ref != other.t_ref: raise AssertionError('t_ref missmatch!')
            return other
        return self.__class__.const(other, self.degree, self.t_ref)

    def __repr__(self) -> str:
        '''
        repr method, i.e. representation method.

        **returns**

            - a string representation of 'self'
        '''
        return f'Polynomial(coeff = {self.coeff}, t_ref = {self.t_ref})'

    def __str__(self) -> str:
        '''
        as of yet essentially an alias to repr
        '''
        return self.__repr__()

    def __call__(self, t: float, t_ref_alternative: Optional[float] = None) -> float:
        '''
        evaluates the polynomial stored within self via horners method, i.e.

        $$
        p(t) = c_0 + \delta\!t\cdot(c_1 + \delta\!t\cdot(c_1 + \delta\!t\cdot\dots(c_{d-1} + \delta\!t\cdot c_d)\dots)),
        $$

        where $\delta\!t = (t - t_{\mathrm{ref}})$

        **args**

            - argument t

        **returns**

            - evaluation of polynomial stored within self
        '''
        t_ref = t_ref_alternative if (not (t_ref_alternative is None)) else self.t_ref
        delta_t: float = t - t_ref
        out: float = 0.0
        for ci in reversed(self.coeff):
            out *= delta_t
            out += ci
        return out

    def __pos__(self) -> Self:
        '''
        the pos method aka +p (or +self) realizes or implements the unary plus operation.
        It essentially is a copy method.

        **returns**

            - a copy of self
        '''
        return self.__class__(coeff = list(self.coeff), t_ref = self.t_ref)

    def __add__(self, other: Self | float) -> Self:
        '''
        the add method aka p + q (or self + other) realizes or implements the bivariate sum.

        **args**

            - other: 2nd summand

        **returns**

            - sum of self and other
        '''
        other: Self = self.ensure(other)
        return self.__class__(coeff = [si + oi for si, oi in zip(self.coeff, other.coeff)], t_ref = self.t_ref)

    def __radd__(self, other: float) -> Self:
        '''
        the right-sided add method aka p + q (or self + other) realizes or implements the bivariate sum,
        given the 1st summand is float.

        **args**

            - other: 1st summand

        **returns**

            - sum of other and self
        '''
        other: Self = self.ensure(other)
        return self.__class__(coeff = [oi + si for si, oi in zip(self.coeff, other.coeff)], t_ref = self.t_ref)


def visualize(p: Polynomial,
              a: Optional[float] = 0.0,
              b: Optional[float] = 1.0,
              resolution: Optional[float] = 0.01) -> None:
    '''
    visualizes instances of Polynomial via matplotlib

    **args**

        - p: instance of Polynomial
        - a: left boundary of plot
        - b: right boundary of plot (b > a)
        - resolution: is the plot resolution (number of polynomial evaluations)
    '''
    if b <= a: raise AssertionError('b > a!')

    import numpy as np

    import matplotlib
    import matplotlib.pyplot as plt

    ts = np.arange(a, b, resolution)
    ps = np.array([p(ti) for ti in ts])

    fig, ax = plt.subplots()
    ax.plot(ts, ps)

    ax.set(xlabel = 't', ylabel = 'p(t)', title = 'polynomial visualization')
    ax.grid()

    plt.show()
