from polynomial.core import Polynomial, visualize


def main():
    t_ref: float = 1.0

    p1 = Polynomial([1.0, 0.0, 1.0], t_ref)  # (t - 1.0)**2 + 1.0
    p2 = Polynomial([-1.0, 0.0, 2.1], t_ref)
    p3 = p1 + p2

    print(f'p1:           {p1}')
    print(f'p2:           {p2}')
    print(f'p3 = p1 + p2: {p3}')

    print(f'p3(3.0) - 3.1*4: {p3(3.0) - 3.1*4} (should be 0.0)')

    visualize(p1, b = 2.0)


if __name__ == '__main__': main()
